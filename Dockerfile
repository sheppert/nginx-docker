FROM nginx:1.25.4

RUN apt-get update && apt-get install nano htop cron software-properties-common msmtp certbot python3-certbot-nginx -y

RUN apt-get remove exim4 -y

RUN ln -sfn /usr/bin/msmtp /usr/sbin/sendmail

COPY start.sh /start.sh

RUN chmod 700 /start.sh

CMD ["sh", "/start.sh"]
